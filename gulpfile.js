/**
 * @file - All automated tasks
 */

const gulp  = require('gulp');
const gutil = require('gulp-util');
const eslint = require('gulp-eslint');

/* Application back-end folder paths */
const serverPaths = {
    ROOT:           './server',
    CONFIG:         './server/config',
    MODELS:         './server/models',
    CONTROLLERS:    './server/controllers',
    ROUTES:         './server/routes',
    MIGRATIONS:     './server/migrations',
    SEEDERS:        './server/seeders'
};

/* Application front-end folder paths */
const publicPaths = {
    ROOT: './public'
};

// Default test task, just logs a message :)
gulp.task('default', () => {
    return gutil.log('Gulp says: hi!');
});

gulp.task('lint', () => {
    return gulp.src(serverPaths.ROOT + '/**/*.js')
        .pipe(eslint())
        .pipe(eslint.format())
        // Brick on failure but report errors for all files
        .pipe(eslint.failAfterError());
});

gulp.task('lint-fix', () => {
    const options = {
        env: { 'es6': true, 'node': true },
        parserOptions: { 'sourceType': 'module' },
        rules: {
            'no-multi-spaces': [2, {
                'ignoreEOLComments': true,
                'exceptions': { 'VariableDeclarator': true, 'Property': true }
            }],
            'capitalized-comments': [1, 'always', { 'ignoreInlineComments': true }],
            'comma-dangle': [2, 'never'],
            'comma-style': [2, 'last'],
            'keyword-spacing': [1, { 'before': true, 'after': true }],
            'lines-around-comment': [1, { 'beforeBlockComment': true }],
            'no-trailing-spaces': [2, { 'ignoreComments': false }],
            'no-whitespace-before-property': 2,
            'quotes': [2, 'single'],
            'semi': [2, 'always'],
            'semi-style': [2, 'last'],
            'spaced-comment': [1, 'always'],
            'no-var': 2
        },
        useEslintrc: false,
        fix: true
    };

    return gulp.src(serverPaths.ROOT + '/**/*.js')
        .pipe(eslint(options))
        .pipe(eslint.format())
        .pipe(gulp.dest(serverPaths.ROOT));
});
Relational Playground
===========

This is small simple back-end project which uses **MSSql**, **Node** and **Express**.
It also has two example gulp tasks loaded for linting, using ESLint. 

### First steps
* Clone the repo

    ```shell
    git clone https://gitlab.com/SGHDEV/la-para.git
    cd la-para
    ```
* Install dependencies

    ```shell
    npm install
    ```
* Create an **.env** file with the following lines:

    ```
    SGH_DB_USERNAME=username
    SGH_DB_PASSWORD=password
    SGH_DB_INSTANCE=instance_name
    ```
Replace *username*, *password* and *instance_name* with the ones corresponding to your **SQL Server** instance.

&nbsp;
### To run **DEVELOPMENT**

* Start the server

    ```shell
    node server/server.js
    ```
The server starts in **development** by default, unless the *environment variables* are set.

&nbsp;
### To run **PRODUCTION**
* Set the *environment variables* to those of your **SQL Server** instance.

    * *in PowerShell:*

    ```powershell
    $env:SGH_ENV = 'production'
    $env:SGH_DB_USERNAME = 'usuario'
    $env:SGH_DB_PASSWORD = 'password'
    $env:SGH_DB_INSTANCE = 'instancia'
    ```

    * *in cmd:*

    ```shell
    set SGH_ENV = 'production'
    ...
    ```

    * *in bash:*

    ```shell
    SGH_ENV = 'production'
    ...
    ```

&nbsp;
### Errors
If database connection fails with:
> Failed to connect to localhost:undefined in ...

then probably **SQL Browser** service is **STOPPED**. Start it from an elevated console

```shell
net start SQLBrowser
```

or from the Task Manager, under the *Servicies* tab.
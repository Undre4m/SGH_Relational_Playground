'use strict';

/**
 * Module dependencies
 */
const Errors = require('../config/api-errors');
const Publication = require('../models').Publication;

/* Get all web publications */
module.exports.getAll = function (req, res) {
    Publication.findAll().then(result => {
        res.send(result);
    }).catch(() => {
        res.status(500).send(Errors.DATABASE_ERROR);
    });
};
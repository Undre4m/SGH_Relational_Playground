'use strict';

/**
 * @file - Project configuration consolidating the default and environment specific configurations
 */

const _ = require('lodash');
const path = require('path');

module.exports = _.merge(
    require(path.join(__dirname, '/env/default.js')),
    require(path.join(__dirname, '/env/', process.env.SGH_ENV + '.js')) || {}
);
'use strict';

/**
 * @file - Configurations used in PRODUCTION
 */

const path = require('path');

module.exports = {
    port: process.env.SGH_PORT || 8080,
    logFormat: 'common',
    logFile: path.join(__dirname, '/../../logs/server-prod.log'),
    db: {
        database: 'lapara',
        host: 'localhost',
        username: process.env.SGH_DB_USERNAME,
        password: process.env.SGH_DB_PASSWORD,
        dialect: 'mssql',
        dialectOptions: {
            instanceName: process.env.SGH_DB_INSTANCE
        }
    }
};
'use strict';

/**
 * @file - Configuration defaults and common to ALL environments
 */

module.exports = {
    port: process.env.SGH_PORT || 8080,
    logFormat: 'dev',
    logFile: null,
    db: {
        logging: false,
        use_env_variable: false,
        migrationStorageTableName: 'SEQUELIZE_META',
        define: {
            underscored: true,
            underscoredAll: true,
            freezeTableName: false
        }
    }
};
'use strict';

/**
 * @file - Configurations used in DEVELOPMENT
 */

require('dotenv').config();

module.exports = {
    port: process.env.SGH_PORT || 8080,
    db: {
        logging: console.log,
        database: 'lapara-dev',
        host: 'localhost',
        username: process.env.SGH_DB_USERNAME || 'sa',
        password: process.env.SGH_DB_PASSWORD || null,
        dialect: 'mssql',
        dialectOptions: {
            instanceName: process.env.SGH_DB_INSTANCE || 'SQLEXPRESS',
            connectTimeout: 1200
        }
    }
};
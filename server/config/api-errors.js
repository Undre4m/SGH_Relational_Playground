'use strict';

/**
 * @file - Standarized internal error codes and short descriptive messages to be returned from api endpoints
 */

const Errors = {
    INVALID_DATA: {
        code: 4000,
        message: 'Invalid request data.'
    },
    UNAUTHENTICATED: {
        code: 4001,
        message: 'Authentication required.'
    },
    UNAUTHORIZED: {
        code: 4003,
        message: 'Unauthorized operation.'
    },
    RESOURCE_NOT_FOUND: {
        code: 4004,
        message: 'The requested resource does not exists.'
    },
    DATABASE_ERROR: {
        code: 5005,
        message: 'Database operation failed.'
    },
    UNKNOWN: {
        code: 5000,
        message: 'Does not compute.'
    },
    NOT_IMPLEMENTED: {
        code: 5001,
        message: 'Not implemented.'
    }
};

module.exports = Errors;
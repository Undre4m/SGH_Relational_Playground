'use strict';
module.exports = function (sequelize, DataTypes) {
    const Publication = sequelize.define('Publication', {
        title: DataTypes.STRING,
        content: DataTypes.STRING
    }, {
        underscored: true,
        classMethods: {
            associate: function (models) {
                // Associations can be defined here
            }
        }
    });
    return Publication;
};
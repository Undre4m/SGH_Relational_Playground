'use strict';

/**
 * @file - Collect all model definitions and associate them accordingly.
 *         Configure the connection to the database and test it.
 */

const fs        = require('fs');
const path      = require('path');
const Sequelize = require('sequelize');
const chalk     = require('chalk');
const basename  = path.basename(module.filename);
const db_config = require('../config/config').db;
const db        = {};

let sequelize;
if (db_config.use_env_variable) {
    sequelize = new Sequelize(process.env[db_config.use_env_variable]);
} else {
    sequelize = new Sequelize(db_config.database, db_config.username, db_config.password, db_config);
}

fs
    .readdirSync(__dirname)
    .filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
    .forEach(file => {
        const model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

sequelize
    .authenticate()
    .then(() => {
        console.log(chalk.bold.green('Connection to database alive.'));
    })
    .catch(err => {
        console.log(chalk.bold.red('Unable to connect to the database ~', err.message));
        // Terminate server process.
        process.exit(1);
    });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
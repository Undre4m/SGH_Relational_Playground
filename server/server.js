'use strict';

const express       = require('express');
const bodyParser    = require('body-parser');
const cookieParser  = require('cookie-parser');
const session       = require('express-session');
const logger        = require('morgan');
const chalk         = require('chalk');

const fs            = require('fs');
const mkdirp        = require('mkdirp');

// Set environment variable and get project configuration
process.env.SGH_ENV = process.env.SGH_ENV || 'development';
const config = require('./config/config');

// Set up the express application
const app = express();

// Declare port number
app.set('port', config.port);

// Log incoming requests, stream to file if so required
let logStream = null;

if (config.logFile) {
    mkdirp(config.logFile + '/..', err => {
        if (err)
            console.log(chalk.bold.red('Log directory creation failed!'));
        else {
            // Set file stream
            logStream = fs.createWriteStream(config.logFile, {
                flags: 'a'
            });

            logStream.on('open', () => console.log(chalk.bold.cyan('Streaming server requests to file.')));
            logStream.on('error', () => console.log(chalk.bold.red('Server log stream failed!')));
        }
    });
}

app.use(logger(config.logFormat, {
    stream: logStream
}));

// Parse incoming requests data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Server routes configuration
const routes = require('./routes');

// Mount routes
app.use('/', routes.static);
app.use('/', routes.publication);

// Start app
app.listen(config.port);
console.log(chalk.bold.yellow('Rise and shine, Mister Freeman...'));
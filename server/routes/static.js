'use strict';

/**
 * @file - Static routes
 */

const express = require('express');

/**
 * Export static routes configuration
 */
module.exports = (() => {
    const router = new express.Router();

    /* Greet wanderer */
    router.get('/', (req, res) => res.send('Welcome to the nothingness.'));

    return router;
})();
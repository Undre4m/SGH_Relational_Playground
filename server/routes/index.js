'use strict';

/**
 * @file - Module where all the modular routes to expose are defined
 */

module.exports.static = require('./static');
module.exports.publication = require('./publication');
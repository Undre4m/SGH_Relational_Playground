'use strict';

/**
 * @file - Web publications routes
 */

const express = require('express');
const publicationController = require('../controllers/publication');

/**
 * Export routes
 */
module.exports = (() => {
    const router = new express.Router();

    /* Get all web publications */
    router.get('/api/publication', publicationController.getAll);

    return router;
})();